﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class mttResortController : MonoBehaviour
{
    public AudioClip showtime;
    public AudioClip metalCrusher;
    public static bool play;
    public TextMesh text;
    public static bool text_s;
    public static int dialNumber;
    public Animator animator;

    private string[] dialog =
    {
            "Hola a todos, y\nbienvenidos al\nshow de\nMettaton!\n(Presiona X)",
            "Aqui tienes \nque elegir la\nrespuesta \ncorrecta de \nuna pregunta",
            "Empecemos con\nalgo simple,\nmuevete con\nlas teclas W, A,\nD, y S",
            "Muy, bien,\ndemasiado.\nSUFICIENTE!!!",
            "Contestar las\npreguntas es\nfacil",
            "Solo eliges\nla que mas\nte parezca",
            "Y la\neliges",
            "Ah, antes de \nempezar...",
            "debes de elegir\nsiempre la \nrespuesta \ncorrecta...",
            "de lo contrario,\nrecibiras dano...",
            "y si recibes\ndemasiado...",
            "M O R I R A S",
            "Ahora si,\nempezemos\nesto"
        };

    AudioSource fuenteAudio;

    // Start is called before the first frame update
    void Start()
    {
        dialNumber = 0;
        fuenteAudio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        //Songs
        if (play && fuenteAudio.clip != metalCrusher)
        {
            animator.SetBool("play", true);
            fuenteAudio.clip = metalCrusher;
            fuenteAudio.Play();
        }
        if (!play && fuenteAudio.clip != showtime)
        {
            fuenteAudio.clip = showtime;
            fuenteAudio.Play();
        }
        //Set dialog
        Dialog();
    }

    void Dialog()
    {
        try
        {
            text.text = dialog[dialNumber];
            if (Input.GetKeyDown(KeyCode.X))
            {
                dialNumber++;
                Debug.Log(dialNumber);
            }
        }
        catch (IndexOutOfRangeException)
        {
            play = true;
            animator.SetBool("question", true);
            try
            {
                text.text = questionsController.Questions.questions[questionsController.questionNumber];
            }
            catch (IndexOutOfRangeException)
            {
                SceneManager.LoadScene("win");
            }
        }
    }
}