﻿using UnityEngine;

public class hpController : MonoBehaviour
{
    TextMesh textMesh;
    // Start is called before the first frame update
    void Start()
    {
        textMesh = GetComponent<TextMesh>();
    }

    // Update is called once per frame
    void Update()
    {
        textMesh.text = $"HP {playerController.hp}/30";
    }
}
