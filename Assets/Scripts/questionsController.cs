﻿using UnityEngine;

public class questionsController : MonoBehaviour
{
    public const string Path = "Assets/Scripts/questionsXML.xml";
    public static int questionNumber = 0;
    public int optionNumber;
    public TextMesh textMesh;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (mttResortController.play)
        {
            textMesh.text = Questions.answers[questionNumber, optionNumber];
        }
        if (playerController.right == 1)
        {
            questionNumber++;
            playerController.right = 0;
        }
        else if (playerController.right == 2)
        {
            playerController.hp -= 5;
            playerController.right = 0;
        }

    }

    void OnTriggerEnter2D(Collider2D target)
    {
        textMesh.color = new Color(100, 255, 100);
    }

    void OnTriggerExit2D(Collider2D target)
    {
        textMesh.color = new Color(0, 255, 0);
    }


    public static class Questions
    {
        public static string[] questions = {
                                             "En done\nesta el canal\nde Panama?",
                                             "Cual es el\nnumero IV?"
                                             };
        public static string[,] answers = {
                                            { "Tamaulipas", "Guanajuato", "Panama", "No existe" },
                                            { "1", "2", "3", "4" }
                                          };
        public static int[] rel = { 2, 3 };
    }
}
